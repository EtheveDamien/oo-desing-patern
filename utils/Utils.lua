return {
  isMouseInBox = function (x, y, bx, by, bw, bh)
    if  x > bx and
        x < bx + bw and
        y > by and
        y <  by + bh then
      return true
    else
      return false
    end
  end,

  --[[
    Inputs :
      - tab : list of Vectors
      tab = {
        { x=1, y=2 },
        { x=3, y=4 },
        { x=5, y=6 }
      }

    Returns :
      - ftab : table that contains numbers
      ftab = {1,2,3,4,5,6}

  ]] 
  lineariseVectorsTable = function (tab)
    local ftab = {}
    for i,v in ipairs(tab) do
      table.insert( ftab, v.x )
      table.insert( ftab, v.y )
    end
    return ftab
  end
}
