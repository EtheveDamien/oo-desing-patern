local SceneManager = require("handlers/SceneManager")
-- global variables
WIDTH, HEIGHT = love.graphics.getDimensions()
TW, TH = 16,16

-- global objects
SM = SceneManager:new()

function love.load()
  love.graphics.setDefaultFilter("nearest")
  SM:load()
end

function love.update(dt)
  SM:update(dt)
end

function love.draw()
  SM:draw()
end

function love.keypressed(key)
  SM:keypressed(key)
end

function love.keyreleased(key)
  SM:keyreleased(key)
end

function love.mousepressed( x, y, button, istouch, presses )
  SM:mousepressed( x, y, button, istouch, presses )
end

function love.mousereleased( x, y, button, istouch, presses )
  SM:mousereleased( x, y, button, istouch, presses )
end
