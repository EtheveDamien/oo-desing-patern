local TransitionsManager = {}
TransitionsManager.__index = TransitionsManager

function TransitionsManager:new()
  local self = {}
  self.callback = function() end
  self.alpha = 0
  self.timer = 0
  self.isTransition = false
  self.current = ""

  self.transitions = {}
  self.transitions["fadeOut"] = {
    load = function()
      self.alpha = 0
    end,
    update = function(dt)
      self.timer = self.timer + dt
      if self.timer >= 0.01 then
        self.alpha = self.alpha + 0.01
        self.timer = 0
      end
      if self.alpha >= 1 then
        self.isTransition = false
        self.alpha = 0
        self.callback()
      end
    end
  }
  self.transitions["fadeIn"] = {
    load = function()
      self.alpha = 1
    end,
    update = function(dt)
      self.timer = self.timer + dt
      if self.timer >= 0.01 then
        self.alpha = self.alpha - 0.01
        self.timer = 0
      end
      if self.alpha <= 0 then
        self.isTransition = false
        self.alpha = 0
        self.callback()
      end
    end
  }

  setmetatable(self, TransitionsManager)
  return self
end

function TransitionsManager:start(name, callback)
  self.current = name
  self.transitions[self.current].load()
  self.isTransition = true
  self.callback = callback or function() end
end

function TransitionsManager:update(dt)
  if self.isTransition then
    self.transitions[self.current].update(dt)
  end
end

function TransitionsManager:draw()
  love.graphics.setColor(0, 0, 0, self.alpha)
  love.graphics.rectangle("fill", 0, 0, WIDTH, HEIGHT)
  love.graphics.setColor(1, 1, 1, 1)
end

return TransitionsManager
