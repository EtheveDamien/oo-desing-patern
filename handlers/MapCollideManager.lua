local Vector = require("objects/Vector")
local MapCollideManager = {}
MapCollideManager.__index = MapCollideManager

function MapCollideManager:new(pMapManager)
  local self = {
    mm = pMapManager,
    lstElement = {},
  }
  setmetatable(self, MapCollideManager)
  return self
end

function MapCollideManager:addElement(_element, scale)
  local obj = {
    body  = _element,
    scale = scale or Vector:new(0,0)
  }
  table.insert(self.lstElement, obj)
end

function MapCollideManager:update(dt)

 



  for i,a in ipairs(self.lstElement) do
    local v = a.body
    local scale = a.scale

    -- pour chaque élément check les colisions avec la map
    local pBg = Vector:new(
      v.position.x + (2 * math.abs(v.scale.x)) + (scale.x * math.abs(v.scale.x)),
      v.position.y + v.dimension.y - (scale.y * v.scale.y)
    )
    local pBd = Vector:new(
      v.position.x + v.dimension.x - (2 * math.abs(v.scale.x)) - (scale.x * math.abs(v.scale.x)),
      v.position.y + v.dimension.y - (scale.y * v.scale.y)
    )
    local pHg = Vector:new(
      v.position.x + (2 * math.abs(v.scale.x)) + (scale.x * math.abs(v.scale.x)),
      v.position.y + (scale.y * v.scale.y)
    )
    local pHd = Vector:new(
      v.position.x + v.dimension.x - (2 * math.abs(v.scale.x)) - (scale.x * math.abs(v.scale.x)),
      v.position.y + (scale.y * v.scale.y)
    )
    local pDh = Vector:new(
      v.position.x + v.dimension.x - (scale.x * math.abs(v.scale.x)),
      v.position.y + (3.8 * v.scale.y) + (scale.y * v.scale.y)
    )
    local pDb = Vector:new(
      v.position.x + v.dimension.x - (scale.x * math.abs(v.scale.x)),
      v.position.y +  v.dimension.y - (3.8 * v.scale.y) - (scale.y * v.scale.y)
    )
    local pGh = Vector:new(
      v.position.x + (scale.x * math.abs(v.scale.x)),
      v.position.y + (3.8 * v.scale.y) + (scale.y * v.scale.y)
    )
    local pGb = Vector:new(
      v.position.x + (scale.x * math.abs(v.scale.x)),
      v.position.y + v.dimension.y - (3.8 * v.scale.y) - (scale.y * v.scale.y)
    )

    local checkGround = Vector:new(
      v.position.x + v.dimension.x/2,
      v.position.y + v.dimension.y - (scale.y * v.scale.y) + 3
    )

    local bg = self.mm:getTileAt(pBg)
    local bd = self.mm:getTileAt(pBd)
    local hg = self.mm:getTileAt(pHg)
    local hd = self.mm:getTileAt(pHd)
    local dh = self.mm:getTileAt(pDh)
    local db = self.mm:getTileAt(pDb)
    local gh = self.mm:getTileAt(pGh)
    local gb = self.mm:getTileAt(pGb)

    -- Coll B
    if bg ~= nil and v.position.y + v.dimension.y - (scale.y*v.scale.y) > bg.position.y and
      bg.isSolid == true 
      then
        v.velocity.y = 0
        v.position.y = bg.position.y - v.dimension.y + (scale.y*v.scale.y)
        v.isGround = true
    end
    if bd ~= nil and  v.position.y + v.dimension.y - (scale.y*v.scale.y) > bd.position.y and
      bd.isSolid == true 
      then
        v.velocity.y = 0
        v.position.y = bd.position.y - v.dimension.y + (scale.y*v.scale.y)
        v.isGround = true
    end

    if bg ~= nil and  bg.isSolid == false and bd ~= nil and  bd.isSolid == false then -- pour anim jump et tomber tout le temps
      v.isGround = false
    end

    -- Coll H
    if hg ~= nil and  v.position.y + (scale.y*v.scale.y) < hg.position.y  + hg.dimension.y and
      hg.isSolid == true and v.velocity.y <= 0
      then
        v.velocity.y = 0
        v.isColideUp = true
        v.position.y = hg.position.y + hg.dimension.y - (scale.y*v.scale.y)
    end
    if hd ~= nil and v.position.y + (scale.y*v.scale.y) < hd.position.y  + hd.dimension.y and
      hd.isSolid == true and v.velocity.y <= 0
      then
        v.velocity.y = 0
        v.isColideUp = false
        v.position.y = hd.position.y + hd.dimension.y - (scale.y*v.scale.y)
    end
    
    if hg ~= nil and hg.isSolid == false and hd ~= nil and hd.isSolid == false then -- pour anim jump et tomber tout le temps
      v.isColideUp = false
    end

    --  Coll D
    if dh ~= nil and v.position.x + v.dimension.x - (scale.x* math.abs(v.scale.x)) > dh.position.x and
      dh.isSolid == true  and v.velocity.x >= 0
      then
        v.velocity.x = 0
        v.position.x = dh.position.x - v.dimension.x + (scale.x* math.abs(v.scale.x)) 
    end
    if db ~= nil and v.position.x + v.dimension.x - (scale.x* math.abs(v.scale.x))  > db.position.x and
      db.isSolid == true and v.velocity.x >= 0
      then
        v.velocity.x = 0
        v.position.x = db.position.x - v.dimension.x + (scale.x* math.abs(v.scale.x)) 
    end

    -- Coll G
    if gh ~= nil and v.position.x + (scale.x* math.abs(v.scale.x)) < gh.position.x + gh.dimension.x and
      gh.isSolid == true and v.velocity.x <= 0
      then
        v.velocity.x = 0
        v.position.x = gh.position.x + gh.dimension.x - (scale.x* math.abs(v.scale.x)) 
    end
    if gb ~= nil and v.position.x + (scale.x* math.abs(v.scale.x))  < gb.position.x + gb.dimension.x and
    gb.isSolid == true and v.velocity.x <= 0
    then
      v.velocity.x = 0
      v.position.x = gb.position.x + gb.dimension.x - (scale.x* math.abs(v.scale.x)) 
    end
  end
end

function MapCollideManager:draw()
  love.graphics.setColor(1,0,0,1)
  for i,a in ipairs(self.lstElement) do
    local v = a.body
    local scale = a.scale
    -- draw bas
    love.graphics.points(
      v.position.x + (2 * math.abs(v.scale.x)) + (scale.x * math.abs(v.scale.x)),
      v.position.y + v.dimension.y - (scale.y * v.scale.y)
    )
    love.graphics.points(
      v.position.x + v.dimension.x - (2 * math.abs(v.scale.x)) - (scale.x * math.abs(v.scale.x)),
      v.position.y + v.dimension.y - (scale.y * v.scale.y)
    )

    -- draw haut
    love.graphics.points(
      v.position.x + (2 * math.abs(v.scale.x)) + (scale.x * math.abs(v.scale.x)),
      v.position.y + (scale.y * v.scale.y)
    )
    love.graphics.points(
      v.position.x + v.dimension.x - (2 * math.abs(v.scale.x)) - (scale.x * math.abs(v.scale.x)),
      v.position.y + (scale.y * v.scale.y)
    )

    -- draw droit
    love.graphics.points(
      v.position.x + v.dimension.x - (scale.x * math.abs(v.scale.x)),
      v.position.y + (3.8 * v.scale.y) + (scale.y * v.scale.y)
    )
    love.graphics.points(
      v.position.x + v.dimension.x - (scale.x * math.abs(v.scale.x)),
      v.position.y +  v.dimension.y - (3.8 * v.scale.y) - (scale.y * v.scale.y)
    )

    -- draw gauche
    love.graphics.points(
      v.position.x + (scale.x * math.abs(v.scale.x)),
      v.position.y + (3.8 * v.scale.y) + (scale.y * v.scale.y)
    )
    love.graphics.points(
      v.position.x + (scale.x * math.abs(v.scale.x)),
      v.position.y + v.dimension.y - (3.8 * v.scale.y) - (scale.y * v.scale.y)
    )

  end
  love.graphics.setColor(1,1,1,1)
end

return MapCollideManager
