local TransitionsManager = require("handlers/TransitionsManager")

local SceneManager = {}
SceneManager.__index = SceneManager

function SceneManager:new(pInitial)
  local self = {}
  self.list = {}
  self.transition = TransitionsManager:new()

  -- DYNAMIC check in the scenes folder and add all scenes to the list
  local files = love.filesystem.getDirectoryItems("scenes")
  for k, file in ipairs(files) do
    local fileName = file:match("(.+)%..+$")
    self.list[fileName] = require("scenes/"..fileName)
  end

  self.current = self.list[pInitial or "Game"]

  setmetatable(self, SceneManager)
  return self
end

function SceneManager:setScene(_name)
  self.transition:start(
    "fadeOut",
    function()
      self.current = self.list[_name]
      self.current.load()
      self.transition:start("fadeIn")
    end
  )
end

function SceneManager:load()
  self.current.load()
end

function SceneManager:update(dt)
  self.transition:update(dt)
  self.current.update(dt)
end

function SceneManager:draw()
  self.current.draw()
  self.transition:draw()
end

function SceneManager:keypressed(key)
  local callback = self.current.keypressed
  if callback~=nil then callback(key) end
end

function SceneManager:keyreleased(key)
  local callback = self.current.keyreleased
  if callback~=nil then callback(key) end
end

function SceneManager:mousepressed( x, y, button, istouch, presses )
  local callback = self.current.mousepressed
  if callback~=nil then callback(x, y, button, istouch, presses) end
end

function SceneManager:mousereleased( x, y, button, istouch, presses )
  local callback = self.current.mousereleased
  if callback~=nil then callback(x, y, button, istouch, presses) end
end

-- handle other callbacks

return SceneManager
