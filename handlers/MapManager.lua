local Vector = require("objects/Vector")
local Tile = require("objects/Tile")
local isMouseInBox = require("utils/Utils").isMouseInBox

local MapManager = {}
MapManager.__index = MapManager

-- append private method to mapManager
local generateQuads, generateTiles

function MapManager:new(pX, pY)
  local self = {}
  self.tilesheet = nil
  self.mapFile = nil
  self.level = nil
  self.lstQuad = {}
  self.lstTile = {}

  setmetatable(self, MapManager)
  return self
end

function MapManager:changeMap(_num)
  self.mapFile = require("assets/tiled/map".._num) 

  local tilesetName = self.mapFile.tilesets[1].name
  self.tilesheet = love.graphics.newImage("assets/spritesheets/"..tilesetName..'.png')
  self.level = self.mapFile.layers[1].data

  -- generate the quads for the tiles
  generateQuads(self.tilesheet, self.lstQuad)
  
  -- generate the tiles
  generateTiles(self.mapFile, self.level, self.tilesheet, self.lstQuad, self.lstTile)
end

function MapManager:update(dt)
  --[[
  for i,v in ipairs(self.lstTile) do
    v.position.y = v.position.y - 10 * dt
  end
  ]]
end

function MapManager:draw()
  for i,v in ipairs(self.lstTile) do
    v:draw()
  end
end

function MapManager:drawBack()
  love.graphics.setColor(0.8,0.8,0.8)
  love.graphics.rectangle('fill',0,0,self.mapFile.width * TH, self.mapFile.height * TH)
  love.graphics.setColor(1,1,1)
end

function MapManager:getTileAt(position)
  for i,v in ipairs(self.lstTile) do
    if isMouseInBox(
      position.x,
      position.y,
      v.position.x,
      v.position.y,
      v.dimension.x,
      v.dimension.y
    ) then
      return v
    end
  end
end


-- private method that create the quads
function generateQuads(tilesheet, quadList)
  local tileNumber = Vector:new(
    tilesheet:getWidth() / TW,
    tilesheet:getHeight() / TH
  )
  
  for line=0,tileNumber.y - 1,1 do
    for coll=0, tileNumber.x - 1,1  do
      local quad = love.graphics.newQuad(
        coll*TW,line*TH,
        TW,TH,
        tilesheet:getDimensions()
      )
      table.insert(quadList, quad)
    end
  end
end


-- private method that create the tiles
function generateTiles(mapFile, tileLevel, tilesheet, quadList, tileList)
   -- create tiles
   local id = 1
   for ligne=1, mapFile.height do
     for coll=1, mapFile.width do
       local numberItem = tileLevel[((ligne -1) * mapFile.width + coll)]
       local myTile = Tile:new(
         (coll-1)*TW, (ligne-1)*TH,
         tilesheet, quadList[numberItem],
         numberItem,
         id
       )
       if numberItem ~= 0 and numberItem ~= 11 and numberItem ~= 12 and numberItem ~= 13 then myTile.isSolid = true end
       table.insert(tileList, myTile)
       id = id + 1
     end
   end
end

return MapManager
