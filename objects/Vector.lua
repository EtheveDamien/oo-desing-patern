local Vector = {}
Vector.__index = Vector
Vector.__tostring = function(self)
  return "Vector: " .. self.x .. "," .. self.y
end
Vector.__add = function(a, b)
  return Vector:new(a.x + b.x, a.y + b.y)
end
Vector.__sub = function(a, b)
  return Vector:new(a.x - b.x, a.y - b.y)
end
Vector.__mul = function(a, b)
  return Vector:new(a.x * b, a.y * b)
end
Vector.__div = function(a, b)
  return Vector:new(a.x / b, a.y / b)
end
Vector.__eq = function(a, b)
  return a.x == b.x and a.y == b.y
end
Vector.__unm = function(a)
  return Vector:new(-a.x, -a.y)
end
Vector.__len = function(a)
  return math.sqrt(a.x * a.x + a.y * a.y)
end
Vector.__mod = function(a, b)
  return Vector:new(a.x % b, a.y % b)
end
Vector.__pow = function(a, b)
  return Vector:new(a.x ^ b, a.y ^ b)
end
Vector.__concat = function(a, b)
  return a.x .. "," .. a.y .. " " .. b.x .. "," .. b.y
end
Vector.__call = function(a, b)
  return Vector:new(a.x * b, a.y * b)
end

function Vector:new(pX, pY)
  local self = {}
  self.x = pX or 0
  self.y = pY or 0
  setmetatable(self, Vector)
  return self
end

return Vector
