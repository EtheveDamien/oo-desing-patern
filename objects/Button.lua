local Vector = require("objects/Vector")
local isMouseInBox = require("utils/Utils").isMouseInBox

local Button = {}
Button.__index = Button

function Button:new(pPosition, pDimension, pCallbackPressed, pCallbackReleased)
  local self = {}
  self.position = pPosition
  self.dimension = pDimension
  self.callbackPressed = pCallbackPressed or function () end
  self.callbackReleased = pCallbackReleased or function () end
  self.isPressed = false

  setmetatable(self, Button)
  return self
end

function Button:setPosition(position)
  self.position = position
end

function Button:draw()
  if self.isPressed then
    love.graphics.rectangle(
      "fill",
      self.position.x,
      self.position.y,
      self.dimension.x,
      self.dimension.y
    )
  else
    love.graphics.rectangle(
      "line",
      self.position.x,
      self.position.y,
      self.dimension.x,
      self.dimension.y
    )
  end
end

function Button:mousepressed(x, y)
  local isInside = isMouseInBox(
    x,y, 
    self.position.x, self.position.y, 
    self.dimension.x, self.dimension.y
  )
  if isInside then
    self.callbackPressed()
    self.isPressed = true
  end
end

function Button:mousereleased(x, y)
  if self.isPressed then
    self.callbackReleased()
    self.isPressed = false
  end
end

return Button
