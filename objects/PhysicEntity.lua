local Vector = require("objects/Vector")
local Sprite = require("objects/Sprite")

local PhysicEntity = {}
PhysicEntity.__index = PhysicEntity

-- setup inheritance
setmetatable(PhysicEntity, {__index = Sprite})

function PhysicEntity:new(pX, pY, ...)
  local self = Sprite:new(pX, pY, ...)
  self.gravity = 400
  self.friction = 0.8
  self.speed = 2 --1.3
  self.maxSpeed = 60 
  self.maxSpeedY = 200
  self.isGround = true

  self.superSpriteUpdate = self.update

  setmetatable(self, PhysicEntity)
  return self
end

function PhysicEntity:update(dt)
  self.velocity.y = self.velocity.y + self.gravity * dt
  self:superSpriteUpdate(dt)

  -- velocity limitation
  if self.velocity.x >= self.maxSpeed then
    self.velocity.x = self.maxSpeed
  end
  if self.velocity.x <= -self.maxSpeed then
    self.velocity.x = -self.maxSpeed
  end
  if self.velocity.y >= self.maxSpeedY then
    self.velocity.y = self.maxSpeedY
  end

  -- friction
  if self.isKeyPressed == false then
    if self.velocity.x > 2 then
      self.velocity.x = self.velocity.x - self.friction
    elseif self.velocity.x < -2 then
      self.velocity.x = self.velocity.x + self.friction
    else
      self.velocity.x = 0
    end
  end
end

return PhysicEntity
