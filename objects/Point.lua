local Entity = require("objects/Entity")

local Point = {}
Point.__index = Point

-- setup inheritance
setmetatable(Point, {__index = Entity})

function Point:new(pX, pY)
  local self = Entity:new(pX, pY)

  setmetatable(self, Point)
  return self
end

function Point:getDistanceFrom(other)
  local dx = self.position.x - other.position.x
  local dy = self.position.y - other.position.y
  return math.sqrt ( dx * dx + dy * dy )
end

function Point:getAngleFrom(other)
  local rad = math.atan2(
    other.position.x - self.position.x,
    other.position.y - self.position.y
  )
  return math.deg(rad)
end

function Point:draw()
  love.graphics.points(
    self.position.x,
    self.position.y
  )
end

return Point
