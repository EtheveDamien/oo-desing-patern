local Vector = require("objects/Vector")
local Entity = require("objects/Entity")
local Animator = require("objects/Animator")

local Sprite = {}
Sprite.__index = Sprite

-- setup inheritance
setmetatable(Sprite, {__index = Entity})

function Sprite:new(pX, pY, pSpriteSheet, pNbX, pNbY)
  local self = Entity:new(pX, pY)
  self.angle = 0
  self.scale  = Vector:new(1,1)
  self.nbImageInTheSpritesheet = Vector:new(pNbX, pNbY) or nil
  self.anims = {} -- contains all animation
  self.spriteSheet = pSpriteSheet
  self.currentAnim = nil

  -- super methods
  self.superEntityUpdate = self.update

  setmetatable(self, Sprite)
  self:setScale(self.scale)
  return self
end

function Sprite:setScale(scale)
  self.scale  =  scale
  if self.spriteSheet ~= nil then
    self.dimension = Vector:new(
      (self.spriteSheet:getWidth()/self.nbImageInTheSpritesheet.x) * self.scale.x,
      (self.spriteSheet:getHeight()/self.nbImageInTheSpritesheet.y) * self.scale.y
    )
  else
    self.dimension = Vector:new(TW * self.scale.x, TH*self.scale.y)
  end
end

function Sprite:rotate(r)
  self.angle = r
end

function Sprite:addAnim(pName, pStart, pEnd, pDuration)
  local anim = Animator:new(
    self.spriteSheet, 
    self.dimension,
    Vector:new(pStart, pEnd),
    pDuration or 1
  )
  self.anims[pName] = anim
end

function Sprite:playAnim(pName)
  if self.currentAnim ~= pName and next(self.anims) ~= nil and self.currentAnim ~= nil then
    self.anims[self.currentAnim]:stop()
  end
  self.currentAnim = pName
  self.anims[self.currentAnim]:play()
end

function Sprite:update(dt)
  self:superEntityUpdate(dt)

  if next(self.anims) ~= nil and self.currentAnim ~= nil then
    self.anims[self.currentAnim]:update(dt)
  end
end

function Sprite:draw()
  local mPosition = Vector:new(self.position.x + self.dimension.y/2, self.position.y + self.dimension.y/2)
  if next(self.anims) ~= nil and self.currentAnim ~= nil then
    self.anims[self.currentAnim]:draw(
      mPosition,
      self.angle,
      self.scale
    )
  else
    -- if there is no anim
    love.graphics.rectangle(
      "line",
      self.position.x,
      self.position.y,
      self.dimension.x, self.dimension.y
    )
  end
  
end

return Sprite
