local Vector = require("objects/Vector")

local Animator = {}
Animator.__index = Animator

function Animator:new(pSpriteSheet, pDimension, pNumAnim, pDuration)
  local self = {
    spriteSheet   = pSpriteSheet,
    numAnim       = pNumAnim,   -- vector
    dimension     = pDimension,  -- vector
    duration      = pDuration or 1,
    currentTime   = 0,
    isRunning     = false,
    quads         = {}
  }

  local currentSprite = 0
  for y = 0, self.spriteSheet:getHeight() - self.dimension.y, self.dimension.y do
    for x = 0, self.spriteSheet:getWidth() - self.dimension.x, self.dimension.x do
      currentSprite = currentSprite + 1
      
      if currentSprite >= self.numAnim.x and currentSprite <= self.numAnim.y then
        table.insert(
          self.quads,
          love.graphics.newQuad(
            x, y,
            self.dimension.x, self.dimension.y,
            self.spriteSheet:getDimensions()
          )
        )
      end
    end
  end

  setmetatable(self, Animator)
  return self
end

function Animator:play()
  self.isRunning = true
end

function Animator:stop()
  self.isRunning = false
  self.currentTime = 0
end

function Animator:update(dt)
  if self.isRunning then
    self.currentTime = self.currentTime + dt
    if self.currentTime >= self.duration then
      self.currentTime = self.currentTime - self.duration
    end
 end
end

function Animator:draw(position, angle, scale)
  local spriteNum = math.floor(self.currentTime / self.duration * #self.quads) + 1
  
  if self.quads[spriteNum] ~= nil then -- fix if we move the game
    love.graphics.draw(
      self.spriteSheet,
      self.quads[spriteNum],
      position.x, position.y,
      math.rad(angle),
      scale.x, scale.y,
      self.dimension.x / 2, self.dimension.y /2 
    )
  end
end

return Animator
