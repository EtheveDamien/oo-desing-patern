local Vector = require("objects/Vector")

local Entity = {}
Entity.__index = Entity

function Entity:new(pX, pY)
  local self = {}
  self.position = Vector:new(pX, pY)
  self.velocity = Vector:new(0, 0)
  setmetatable(self, Entity)
  return self
end

function Entity:setPosition(position)
  self.position = position
end

function Entity:getPosition()
  return self.position
end

function Entity:update(dt)
  self.position = self.position + self.velocity * dt
end

return Entity
