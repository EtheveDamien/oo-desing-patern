local Vector = require("objects/Vector")

local Camera = {}
Camera.__index = Camera

function Camera:new()
  local self = {}
  self.position = Vector:new(0, 0)
  self.scale = Vector:new(1, 1)
  self.rotation = 0
  setmetatable(self, Camera)
  return self
end

function Camera:set()
  love.graphics.push()
  love.graphics.rotate(-self.rotation)
  love.graphics.scale(1 / self.scale.x, 1 / self.scale.y)
  love.graphics.translate(-self.position.x, -self.position.y)
end

function Camera:unset()
  love.graphics.pop()
end

function Camera:move(dx, dy)
  self.position.x = self.position.x + (dx or 0)
  self.position.y = self.position.y + (dy or 0)
end

function Camera:rotate(dr)
  self.rotation = self.rotation + dr
end

function Camera:reScale(sx,sy)
  sx = sx or 1
  self.scale.x = self.scale.x * sx
  self.scale.y = self.scale.y * (sy or sx)
end

function Camera:setX(value)
  if self._bounds then
    self.position.x = math.clamp(value, self._bounds.x1, self._bounds.x2)
  else
    self.position.x = value
  end
end

function Camera:setY(value)
  if self._bounds then
    self.position.y = math.clamp(value, self._bounds.y1, self._bounds.y2)
  else
    self.position.y = value
  end
end

function Camera:setPosition(x, y)
  if x then self:setX(x) end
  if y then self:setY(y) end
end

function Camera:setScale(sx, sy)
  self.scale.x = sx or self.scale.x
  self.scale.y = sy or self.scale.y
end

function Camera:getBounds()
  return self._bounds.x1, self._bounds.y1, self._bounds.x2, self._bounds.y2
end

function Camera:setBounds(x1, y1, x2, y2)
  self._bounds = { x1 = x1, y1 = y1, x2 = x2, y2 = y2 }
end

function Camera:getMousePosition(x,y)
  local mx =  x * self.scale.x + self.position.x 
  local my = y * self.scale.y + self.position.y 
  return mx, my
end

return Camera
