local Vector = require("objects/Vector")

local ProgressBar = {}
ProgressBar.__index = ProgressBar

function ProgressBar:new(value, position, dimension, min, max)
  local self = {}
  self.max = max or 100
  self.min = min or 0
  self.value = value or 0
  self.dimension = dimension or Vector:new(100,10)
  self.position = position or Vector:new(0, 0)

  setmetatable(self, ProgressBar)
  return self
end

function ProgressBar:setPosition(position)
  self.position = position
end

function ProgressBar:resize(pValue)
  self.max = pValue
end

function ProgressBar:update(dt)
  if self.value >= self.max then
    self.value = self.max
  elseif  self.value <= self.min then
    self.value = self.min
  end
end

function ProgressBar:draw()
  -- draw back
  love.graphics.setColor(0.1,0.1,0.1,0.5)
  love.graphics.rectangle("fill", self.position.x, self.position.y, self.dimension.x, self.dimension.y)
  
  -- draw front
  local state = (self.value * self.dimension.x) / self.max
  love.graphics.setColor(0.4,1,0.4,0.7)
  if state -8 >= 0 then 
    love.graphics.rectangle(
      "fill", 
      self.position.x + 2, self.position.y + 2,
      state - 4, self.dimension.y - 4
    )
  end

  love.graphics.setColor(1,1,1,1)
end

return ProgressBar
