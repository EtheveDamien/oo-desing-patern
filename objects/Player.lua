local PhysicEntity = require("objects/PhysicEntity")
local Vector = require("objects/Vector")

local Player = {}
Player.__index = Player

-- setup inheritance
setmetatable(Player, {__index = PhysicEntity})

function Player:new(pX, pY)
  local image = love.graphics.newImage("assets/spritesheets/stickman.png")
  local self = PhysicEntity:new(pX, pY, image, 10, 3)
  self:addAnim("idle", 11, 15, 0.7)
  self:addAnim("run", 1,10, 0.6)
  self:addAnim("jump", 16, 20, 0.6)
  self:addAnim("fall", 21, 30, 0.6)
  self:playAnim("idle")
  self.isColideUp = true

  -- super methods
  self.superPhysicEntityUpdate = self.update

  setmetatable(self, Player)
  return self
end

function Player:update(dt)
  --print(self.isGround, self.isColideUp)

  -- animations
  if math.abs(self.velocity.x) >= 2 and math.abs(self.velocity.y) >= 0 and math.abs(self.velocity.y) <= 1 then 
    self:playAnim("run")
  elseif self.velocity.y <= 2 and self.isGround == false then
    self:playAnim("jump")
  elseif self.velocity.y >= 2 then
    self:playAnim("fall")
  else
    self:playAnim("idle")
  end

  -- keyboard inputs
  if love.keyboard.isDown("left") then
    self.velocity.x = self.velocity.x - self.speed
    self.scale.x = - math.abs(self.scale.x)
    self.isKeyPressed = true
  end
  if love.keyboard.isDown("right") then
    self.velocity.x = self.velocity.x + self.speed
    self.scale.x = math.abs(self.scale.x)
    self.isKeyPressed = true
  end

  self:superPhysicEntityUpdate(dt)
end

function Player:keypressed(key)
  if key == "space" and self.isGround and self.isColideUp == false then
    self.velocity.y = -200
    self.isGround = false
  end
end

function Player:keyreleased(key)
  self.isKeyPressed = false
end

return Player
