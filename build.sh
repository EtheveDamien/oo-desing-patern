#!/bin/sh
# prerequisites : luaJit

LOVE_DOWNLOAD_LINK="https://github.com/love2d/love/releases/download/11.3/love-11.3-win64.zip"
FILE_NAME_ZIP=${LOVE_DOWNLOAD_LINK##*/}
FILE_NAME=${FILE_NAME_ZIP%.*}
WD=`pwd`

# (1) prepare build
rm -rf build
mkdir -p build
cd build
mkdir LOVE exe
wget $LOVE_DOWNLOAD_LINK
cd LOVE
unzip ../${FILE_NAME_ZIP}
cd $FILE_NAME && mv ./* ../ && cd .. && rm -rf $FILE_NAME
cd ..
rm -rf $FILE_NAME_ZIP
cd ..

# (2) make love file
TEMP=`mktemp -d` # create a temporary directory
cp -Rv ./* $TEMP # copy your source code to temp
cd $TEMP # move to temp
rm -rf build
ls -la
# obfuscate the code
for file in $(find . -iname "*.lua") ; do
 luajit -b ${file} ${file}
done
zip -r ./build.zip ./* -x ./build.sh -x ./build
mv build.zip game.love
mv game.love $WD/build
cd $WD/build

# (3) build windows executable
cat LOVE/love.exe ./game.love > exe/game.exe
cp -r LOVE/* ./exe
cd exe
rm changes.txt readme.txt lovec.exe love.exe
cd ..
