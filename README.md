# OO desing patern

This project is a game engine made using love2D.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

- LOVE 11.4 (Mysterious Mysteries)

### Installing

```bash
git clone git@gitlab.com:EtheveDamien/oo-desing-patern.git
```

### Running

Running the project.

```bash
cd oo-desing-patern
love .
```

## Authors

- **Damien ETHÈVE** - *DevOps* - [gitlab](https://gitlab.com/EtheveDamien)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
