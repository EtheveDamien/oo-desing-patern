local Player = require("objects/Player")
local MM = require("handlers/MapManager")
local MCM = require("handlers/MapCollideManager")
local Camera = require("objects/Camera")
local Slider = require("objects/Slider")
local Vector = require("objects/Vector")
local Box = require("objects/PhysicEntity")

local Game = {}

local p -- player object
local mm -- map manager object
local mcm -- map collide manager object
local cam  -- camera object
local slider -- slider object

function Game.load()
  love.graphics.setBackgroundColor(185/255, 253/255, 250/255)
  p = Player:new(70, 100)
  mm = MM:new()
  cam = Camera:new()
  mm:changeMap(3)
  cam:reScale(0.3,0.3)
  mcm = MCM:new(mm)

  mcm:addElement(
    p,
    Vector:new(2,0) -- rescale colider box
  )

  slider = Slider:new(
    Vector:new(100,50),
    100,
    0.5,
    0.1,
    1.5,
    function (v)  end,
    {
      width=10,
      orientation="horizontal",
      track="line",
      knob="circle"
    }
  )
end

function Game.update(dt)
  p:update(dt)

  slider:update()
  local s = slider:getValue()
  --box:setScale(Vector:new(s,s))
  local dx = cam.position.x - (p.position.x - WIDTH/2 * cam.scale.x) 
  local dy = cam.position.y - (p.position.y - HEIGHT/2 * cam.scale.y )
  cam:setPosition(
    cam.position.x - (dx * 0.05), --0.06
    cam.position.y - (dy * 0.05)
  )
  mcm:update(dt)
  mm:update(dt)
  
end

function Game.draw()
  cam:set()

  mm:draw()
  p:draw()
  mcm:draw()

  cam:unset()
  love.graphics.setColor(0,0,0,1)
  slider:draw()
  love.graphics.print("fps: "..tostring(love.timer.getFPS( )), WIDTH - 6*TW, 10)
  love.graphics.setColor(1,1,1,1)
end

function Game.keypressed(key)
  p:keypressed(key)
  if(key == "a") then SM:setScene("Menu") end
end

function Game.keyreleased(key)
  p:keyreleased(key)
end

function Game.mousepressed(x,y)
  local mx, my = cam:getMousePosition(x,y)
  -- if the mouse is on the map
  if mx >= 0 and mx <= mm.mapFile.width * TW and my >= 0 and my <= mm.mapFile.height * TH then
    local tile = mm:getTileAt(Vector:new(mx, my))
    tile.num = 2
    tile.isSelected = true
    tile.isSolid = true
  end
end

return Game
