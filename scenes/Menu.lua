local Button = require("objects/Button")
local Vector = require("objects/Vector")
local Slider = require("objects/Slider")

local Menu = {}
local play
local slider
local r = 0

function Menu.load()

  love.graphics.setBackgroundColor(0,0,0,1)
  play = Button:new(
    Vector:new(WIDTH/2 - 100/2, HEIGHT/2 - 50/2),
    Vector:new(100,50),
    function() end,
    function() SM:setScene("Game") end
  )
  slider = Slider:new(
    Vector:new(100,100),
    100,
    0,
    0,
    1,
    function (v)  end,
    {
      width=10,
      orientation="vertical",
      track="line",
      knob="circle"
    }
  )
end

function Menu.update()
  slider:update()
  r = slider:getValue() * 100
end

function Menu.draw()
  play:draw()
  slider:draw()
  love.graphics.circle('fill', 300,300, r)
end

function Menu.keypressed(key)
  SM:setScene("Game")
end

function Menu.mousepressed(x, y, button, istouch, presses)
  play:mousepressed(x, y)
end

function Menu.mousereleased(x, y, button, istouch, presses)
  play:mousereleased(x, y)
end

return Menu
